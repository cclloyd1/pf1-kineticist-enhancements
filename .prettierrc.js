module.exports = {
    semi: true,
    trailingComma: 'all',
    singleQuote: true,
    printWidth: 120,
    tabWidth: 4,
    error: {
        endOfLine: 'crlf',
    },
    quoteProps: 'as-needed',
};
