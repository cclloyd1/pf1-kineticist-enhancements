class SimpleBlast {
    constructor() {}
}

class CompositeBlast extends SimpleBlast {
    constructor() {
        super();
    }
}

class Infusion {
    constructor() {}
}

class FormInfusion extends Infusion {
    constructor() {
        super();
    }
}
