export const ns = 'pf1-kineticist-enhancements';

export const defaultDC = '10 + @classes.kineticist.level + @abilities.con.mod';
