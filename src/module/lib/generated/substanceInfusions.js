export const substanceInfusions = {
    blocking: {
        name: 'Blocking',
        element: ['universal'],
        type: 'substance',
        level: 2,
        burn: 'see below',
        prerequisites: ['ki-pool', 'kinetic-fist'],
        blasts: ['any'],
        save: 'Fortitude partial',
        description:
            "You infuse your elemental energy with kundalini serpent fire.\n\nChoose a chakra you can currently open; you must accept an amount of burn equal to the chakra's number (1 for root, 2 for sacral, and so on). Your unarmed strikes with the infused blast cause the target to suffer the effects of blocking the chosen chakra as per the Block Chakras or Block Upper Chakras feat. You can use this infusion only while also using the kinetic fist form infusion.",
        id: 'blocking',
        prepend: true,
        append: false,
        prependText: 'Blocking',
        appendText: null,
    },
    bowling: {
        name: 'Bowling',
        element: ['aether', 'earth'],
        type: 'substance',
        level: 2,
        burn: 2,
        blasts: ['earth', 'magma', 'metal', 'mud', 'telekinetic'],
        save: null,
        description:
            'You bowl your foes over with the sheer mass of your kinetic blast.\n\nAttempt a trip combat maneuver check against each target damaged by your infused blast, using your Constitution modifier instead of your Strength modifier to determine your Combat Maneuver Bonus.',
        id: 'bowling',
        prepend: true,
        append: false,
        prependText: 'Bowling',
        appendText: null,
    },
    brilliant: {
        name: 'Brilliant',
        element: ['fire'],
        type: 'substance',
        level: 6,
        burn: 4,
        prerequisites: ['flash'],
        blasts: ['blue-flame', 'fire', 'plasma'],
        save: null,
        description:
            'Your kinetic blast leaves an afterglow that banishes the darkness.\n\nTreat each square of the path of your ranged attack kinetic blast, the target square of your melee attack kinetic blast, or the area of your area of effect kinetic blast as the center of a continual flame effect that lasts until the end of your next turn. This effect counts as a 6th-level spell with the light descriptor.',
        id: 'brilliant',
        prepend: true,
        append: false,
        prependText: 'Brilliant',
        appendText: null,
    },
    burning: {
        name: 'Burning',
        element: ['fire'],
        type: 'substance',
        level: 1,
        burn: 1,
        blasts: ['blue-flame', 'fire', 'magma', 'plasma'],
        save: 'Reflex negates',
        description:
            'Your kinetic blast ignites your foes. Whenever an infused blast hits a foe and penetrates its spell resistance, that foe catches on fire, regardless of whether it takes damage. A foe that catches fire takes 1d6 points of fire damage each round until the fire is extinguished. Against a creature on fire from this infusion, any fire kinetic blasts gain a +2 bonus on attack rolls, to DCs, and on caster level checks to overcome spell resistance.',
        id: 'burning',
        prepend: true,
        append: false,
        prependText: 'Burning',
        appendText: null,
    },
    chilling: {
        name: 'Chilling',
        element: ['water'],
        type: 'substance',
        level: 5,
        burn: 3,
        blasts: ['blizzard', 'cold', 'ice'],
        save: 'Fortitude negates',
        description:
            'Your kinetic blast chills your foes to the bone, making their movements sluggish. Whenever an infused blast deals cold damage to a foe, that foe is staggered for [[1]] round.',
        id: 'chilling',
        prepend: true,
        append: false,
        prependText: 'Chilling',
        appendText: null,
    },
    dampening: {
        name: 'Dampening',
        element: ['void'],
        type: 'substance',
        level: 1,
        burn: 1,
        blasts: ['negative', 'void'],
        save: 'Will negates',
        description:
            'Your kinetic blast swirls with darkness, making it harder for your foes to see. This otherwise functions as dazzling infusion.',
        id: 'dampening',
        prepend: true,
        append: false,
        prependText: 'Dampening',
        appendText: null,
    },
    darkness: {
        name: 'Darkness',
        element: ['void'],
        type: 'substance',
        level: 3,
        burn: 2,
        blasts: ['gravity', 'negative', 'void'],
        save: 'none',
        description:
            'Your kinetic blast creates darkness. Treat each square of the path of your ranged attack kinetic blast, the target square of your melee attack kinetic blast, or the area of your area of effect kinetic blast as the center of a darkness effect that lasts until the end of your next turn. This effect counts as a 3rd-level darkness spell.',
        id: 'darkness',
        prepend: true,
        append: false,
        prependText: 'Dark',
        appendText: null,
    },
    'darkness-greater': {
        name: 'Darkness, Greater',
        element: ['void'],
        type: 'substance',
        level: 6,
        burn: 4,
        prerequisites: ['darkness'],
        blasts: ['gravity', 'negative', 'void'],
        save: 'none',
        description:
            'Your blast creates supernatural darkness, as per the darkness infusion, except it acts as deeper darkness and counts as a 6th-level darkness spell.',
        id: 'darkness, greater',
        prepend: true,
        append: false,
        prependText: 'Super Dark',
        appendText: null,
    },
    dazzling: {
        name: 'Dazzling',
        element: ['fire', 'wood'],
        type: 'substance',
        level: 1,
        burn: 1,
        blasts: ['blue-flame', 'fire', 'plasma', 'positive', 'verdant'],
        save: 'Will negates',
        description:
            "Your kinetic blast shines brightly, making it harder for your foes to see. If your blast hits a foe (and penetrates SR if applicable), that foe must succeed at a Will save or be dazzled for 1 minute, whether or not it takes damage from the blast. You can reduce the blast's damage by half to increase the DC of this save by 2.",
        id: 'dazzling',
        prepend: true,
        append: false,
        prependText: 'Dazzling',
        appendText: null,
    },
    disintegrating: {
        name: 'Disintegrating',
        element: ['aether'],
        type: 'substance',
        level: 6,
        burn: 4,
        blasts: ['force'],
        save: 'Fortitude partial; see text',
        description:
            "You can use force to rip your targets apart. Against creatures, your kinetic blast deals double its normal amount of damage, but targets receive a saving throw to reduce the damage to half the blast's normal amount of damage (for a total of 1/4 of the blast's increased damage). Any creature reduced to 0 or fewer hit points by the blast is disintegrated, as the spell disintegrate. You can use the infused blast to destroy force effects or objects as disintegrate, but for each force effect or 10-foot cube of nonliving matter you destroy in this way, you must accept 1 point of burn, which can't be reduced by effects such as infusion specialization or gather power.",
        id: 'disintegrating',
        prepend: true,
        append: false,
        prependText: 'Disintegrating',
        appendText: null,
    },
    draining: {
        name: 'Draining',
        element: ['universal'],
        type: 'substance',
        level: 1,
        burn: 1,
        blasts: ['any-simple'],
        save: 'Fortitude partial; see text',
        description:
            "You can drain elemental energy and matter from your foes to strengthen your next attack. When you use this infusion, your kinetic blast affects only creatures with a subtype matching your kinetic blast's element (for instance, a fire blast would damage only creatures of the fire subtype). Against such creatures, your blast targets touch AC (if it requires an attack roll) and always allows spell resistance. Because you are draining energy from the target, your blast doesnt apply your elemental overflow bonuses or Constitution modifier. The target can attempt a Fortitude save to take 1/4 the normal amount of damage. Draining infusion ignores any damage reduction, resistances, and immunities the creature might possess. If at least one creature fails its saving throw against your draining infusion, you can reduce the total burn cost of any one blast wild talent used before the end of your next turn by 1, or by 2 if you have the supercharge ability. If you use draining infusion again before applying this reduction, you still deal the damage from that draining infusion, but the burn reductions don't stack.",
        id: 'draining',
        prepend: true,
        append: false,
        prependText: 'Draining',
        appendText: null,
    },
    enervating: {
        name: 'Enervating',
        element: ['void'],
        type: 'substance',
        level: 7,
        burn: 4,
        blasts: ['negative', 'void'],
        save: 'Fortitude negates',
        description:
            'Your kinetic blast drains life force. Foes that take damage from your infused blast also take 1 temporary negative level. Negative levels from this infusion fade after 24 hours and never become permanent.',
        id: 'enervating',
        prepend: true,
        append: false,
        prependText: 'Enervating',
        appendText: null,
    },
    entangling: {
        name: 'Entangling',
        element: ['earth', 'water,', 'wood'],
        type: 'substance',
        level: 2,
        burn: 2,
        blasts: [
            'autumn',
            'blizzard',
            'cold',
            'earth',
            'ice',
            'magma',
            'metal',
            'mud',
            'sandstorm',
            'spring',
            'summer',
            'verdant',
            'winter',
            'wood',
        ],
        save: 'Reflex negates',
        description:
            'Your kinetic blast surrounds your foes in elemental matter.\n\nWhenever a blast with this infusion deals damage to a foe, that foe becomes entangled for 1 minute. The foe can remove this condition as a standard action with a successful Escape Artist or Strength check (with the same DC as for saves against your kinetic blast) or by dealing an amount of damage to the entangling matter equal to double your kineticist level (the matter has hardness 0).\n\nIf the foe was already entangled by this infusion and fails its save against a second instance of this infusion, the increased amount of elemental matter fuses to the ground, causing the foe to be rooted in place as though anchored to an immobile object.',
        id: 'entangling',
        prepend: true,
        append: false,
        prependText: 'Entangling',
        appendText: null,
    },
    flash: {
        name: 'Flash',
        element: ['fire'],
        type: 'substance',
        level: 4,
        burn: 3,
        blasts: ['blue-flame', 'fire', 'plasma'],
        save: 'Will negates',
        description:
            "Your kinetic blast is so bright that it blinds your foes. Whenever an infused blast hits a foe and penetrates its spell resistance, that foe must succeed at a Will save or be blinded for [[1]] round, regardless of whether it takes damage from the blast. You can reduce the blast's damage by half to increase the DC of this save by 2.",
        id: 'flash',
        prepend: true,
        append: false,
        prependText: 'Flashing',
        appendText: null,
    },
    foxfire: {
        name: 'Foxfire',
        element: ['fire', 'wood'],
        type: 'substance',
        level: 3,
        burn: 2,
        prerequisites: ['foxfire-or positive blast'],
        blasts: ['blue-flame', 'fire', 'positive'],
        save: 'Will partial',
        description:
            "Your blast leaves behind flickering flames that reveal the target's location as per faerie fire for 1 minute (or until your next turn if the target succeeds at its save). This effect occurs even if the foe doesn't take damage, as long as the attack overcomes spell resistance (if any). You can reduce the blast's damage by half to increase the DC of this saving throw by 2. A creature can remove the revealing flames early by taking a full-round action to extinguish them.",
        id: 'foxfire',
        prepend: true,
        append: false,
        prependText: 'Foxfire',
        appendText: null,
    },
    grappling: {
        name: 'Grappling',
        element: ['universal'],
        type: 'substance',
        level: 5,
        burn: 3,
        blasts: ['any'],
        save: null,
        description:
            "Your cloud, deadly earth, or wall blast grows tendrils that hold your enemies in place. Whenever a creature takes damage from the infused blast, you can attempt a combat maneuver check to have the blast grapple that opponent. Your bonus on this check is equal to 2 + your kineticist level + your Constitution modifier (treat the blast as a Huge creature). The blast only grapples opponents; it can't perform other functions of grapple maneuvers such as pinning or moving the opponent. If the blast is already grappling an opponent, it receives a +5 bonus on its combat maneuver check to maintain the grapple, as normal. You can use this infusion only if you also use the cloud, deadly earth, or wall form infusion.",
        id: 'grappling',
        prepend: true,
        append: false,
        prependText: 'Grappling',
        appendText: null,
    },
    gusting: {
        name: 'Gusting',
        element: ['air'],
        type: 'substance',
        level: 1,
        burn: 1,
        blasts: ['air', 'blizzard', 'sandstorm', 'thunderstorm'],
        save: 'Fortitude negates',
        description:
            'The wind from your infusion causes your blast to act as an instantaneous gust of wind. If your blast has a clear path, you can accept 2 additional points of burn or reduce the damage to 0 in order to cause the gust of wind effect to persist for [[1]] round along that path.',
        id: 'gusting',
        prepend: true,
        append: false,
        prependText: 'Gusting',
        appendText: null,
    },
    maelstrom: {
        name: 'Maelstrom',
        element: ['water'],
        type: 'substance',
        level: 8,
        burn: 4,
        prerequisites: ['extended-range'],
        blasts: ['charged-water', 'water'],
        save: 'Reflex partial',
        description:
            "You create a 20-foot-radius whirling maelstrom in a body of water within 120 feet (the area of the maelstrom does not include any squares in the radius that aren't in the water). Any creature in the maelstrom immediately takes one-quarter your blast's normal damage regardless of whether it succeeds at its save. A creature that succeeds at its Reflex save can choose to exit the area at the closest space. A creature in the maelstrom must succeed at a DC 35 Swim check in order to swim, and any creature that enters the maelstrom or ends its turn in the maelstrom takes half your blast's normal damage. As a free action at the beginning of your turn, you can position each creature in the maelstrom anywhere you choose within the affected area. This effect lasts for a number of rounds equal to your Constitution modifier ([[@abilities.con.mod]]) or until you use this infusion again. You can use grappling infusion with this infusion.",
        id: 'maelstrom',
        prepend: false,
        append: true,
        prependText: null,
        appendText: 'Maelstrom',
        noBlastText: true,
    },
    magnetic: {
        name: 'Magnetic',
        element: ['air', 'earth'],
        type: 'substance',
        level: 3,
        burn: 2,
        blasts: ['charged-water', 'electric', 'metal', 'plasma', 'thunderstorm'],
        save: null,
        description:
            'Your kinetic blast causes your foes to become mildly magnetic.\n\nWhenever an infused blast deals damage to a foe, attacks made with metal weapons (including metal kinetic blasts) against that foe receive a +4 bonus until the end of your next turn, and the foe takes a –4 penalty on Reflex saving throws to avoid metallic objects.',
        id: 'magnetic',
        prepend: true,
        append: false,
        prependText: 'Magnetic',
        appendText: null,
    },
    penetrating: {
        name: 'Penetrating',
        element: ['air', 'fire,', 'water'],
        type: 'substance',
        level: 2,
        burn: 2,
        blasts: ['blue-flame', 'cold', 'electric', 'fire'],
        save: null,
        description:
            "The intensity of your blasts allows you to break through your foes' resistances. Treat your foe's cold, electricity, or fire resistance as if it were 5 lower for the purpose of determining the infused blast's damage; this doesn't stack with the reduction from searing flame.\n\nYou can increase the burn cost of this infusion to further reduce the foe's resistance against the infused blast by 5 per additional point of burn accepted. This has no effect on creatures with immunity.",
        id: 'penetrating',
        prepend: true,
        append: false,
        prependText: 'Penetrating',
        appendText: null,
    },
    photokinetic: {
        name: 'Photokinetic',
        element: ['wood'],
        type: 'substance',
        level: 1,
        burn: 1,
        prerequisites: ['positive-blast'],
        blasts: ['positive'],
        save: null,
        description:
            "Your blast damages living creatures with light. This deals normal damage to undead creatures but also deals minimum damage (1 point of damage per d6, can't be increased by the 9th-level improvement to metakinesis) to living creatures. Undead creatures that are particularly vulnerable to sunlight, such as vampires, take 1 extra point of damage per die from the infused blast.",
        id: 'photokinetic',
        prepend: true,
        append: false,
        prependText: 'Photokinetic',
        appendText: null,
    },
    pulling: {
        name: 'Pulling',
        element: ['void'],
        type: 'substance',
        level: 1,
        burn: 1,
        blasts: ['gravity', 'void'],
        save: 'none',
        description:
            'Your kinetic blast pulls foes toward you. Attempt a drag combat maneuver check against each target damaged by your infused blast (the blast always drags the foe closer to you), using your Constitution modifier instead of your Strength modifier to determine your CMB. This infusion can pull a foe a maximum of 5 feet. You can increase the maximum distance pulled by 5 feet per additional point of burn accepted.',
        id: 'pulling',
        prepend: true,
        append: false,
        prependText: 'Pulling',
        appendText: null,
    },
    'pure-flame': {
        name: 'Pure-Flame',
        element: ['fire'],
        type: 'substance',
        level: 7,
        burn: 4,
        blasts: ['blue-flame'],
        save: null,
        description:
            'You open a direct conduit to the purest elemental fire and send it surging forth. Your infused blast ignores spell resistance.',
        id: 'pure-flame',
        prepend: true,
        append: false,
        prependText: 'Pure-Flame',
        appendText: null,
    },
    pushing: {
        name: 'Pushing',
        element: ['aether', 'air,', 'earth,', 'void,', 'water,', 'wood'],
        type: 'substance',
        level: 1,
        burn: 1,
        blasts: [
            'air',
            'autumn',
            'blizzard',
            'charged-water',
            'earth',
            'gravity',
            'magma',
            'metal',
            'mud',
            'sandstorm',
            'spring',
            'steam',
            'summer',
            'telekinetic',
            'verdant',
            'void',
            'water',
            'winter',
            'wood',
        ],
        save: null,
        description:
            "The momentum of your kinetic blast knocks foes back. Attempt a bull rush combat maneuver check against each target damaged by your infused blast, using your Constitution modifier instead of your Strength modifier to determine your bonus. This infusion can push a foe back by a maximum of 5 feet. You can increase the burn cost of this infusion to increase the maximum distance pushed by 5 feet per additional point of burn accepted. You can't use this infusion with a form infusion such as cloud that causes your kinetic blast to lack a clear direction to push.",
        id: 'pushing',
        prepend: true,
        append: false,
        prependText: 'Pushing',
        appendText: null,
    },
    quenching: {
        name: 'Quenching',
        element: ['water'],
        type: 'substance',
        level: 1,
        burn: 1,
        blasts: ['charged-water', 'water'],
        save: null,
        description:
            'Your blast puts out non-magical fires as if they had been touched by a water elemental. If you increase the burn cost to 2 or reduce the damage to 0, it also dispels magical fires as if you were a water elemental with a number of Hit Dice equal to your kineticist level.',
        id: 'quenching',
        prepend: true,
        append: false,
        prependText: 'Quenching',
        appendText: null,
    },
    'rare-metal': {
        name: 'Rare-Metal',
        element: ['earth'],
        type: 'substance',
        level: 3,
        burn: 2,
        blasts: ['metal'],
        save: null,
        description:
            'You can control the properties of metal, allowing your metal blast to bypass damage reduction as if it were adamantine, cold iron, or silver (your choice). If your target has an unusual type of damage reduction that could be overcome with the correct type of metal (such as DR/gold), you can also select that metal for this purpose. The metal you create with this infusion crumbles away shortly thereafter and has no gp value.',
        id: 'rare-metal',
        prepend: true,
        append: false,
        prependText: 'Rare-Metal',
        appendText: null,
    },
    'shepherd-of-souls': {
        name: 'Shepherd of Souls',
        element: ['universal'],
        type: 'substance',
        level: 7,
        burn: 4,
        blasts: ['any simple'],
        save: 'Will negates',
        description:
            "Those you kill tend to stay dead.\n\nWhen your blast kills a living creature, it helps shepherd the creature's soul to Purgatory, warding against effects of spells that would attempt to restore the creature to life. Spellcasters attempting to bring the creature back from the dead using raise dead or similar magic must succeed at a caster level check (DC = 11 + your kineticist level) or the spell fails and the material component is wasted.",
        id: 'shepherd-of souls',
        prepend: true,
        append: false,
        prependText: 'Shepherding',
        appendText: null,
    },
    slick: {
        name: 'Slick',
        element: ['water'],
        type: 'substance',
        level: 2,
        burn: 2,
        prerequisites: ['slick'],
        blasts: ['blizzard', 'charged-water', 'cold', 'ice', 'water'],
        save: 'Reflex negates',
        description:
            'Your blast leaves behind slippery water or ice in its area for as long as the blast is in that area and for [[1]] round thereafter, making the area difficult terrain and increasing the DC of Acrobatics checks attempted in the area by 5. You can use this infusion only if you also use a form infusion that has an area of effect.',
        id: 'slick',
        prepend: true,
        append: false,
        prependText: 'Slick',
        appendText: null,
    },
    spore: {
        name: 'Spore',
        element: ['wood'],
        type: 'substance',
        level: 5,
        burn: 3,
        blasts: ['autumn', 'spring', 'summer', 'verdant', 'winter', 'wood'],
        save: 'Fortitude negates',
        description:
            'Creatures that take slashing or piercing damage from your blast are infected with spores. If the target fails its Fortitude save, it takes 1d6 points of damage per round for [[10]] rounds as plants and fungi grow out of its body. At the end of that time, the target is exposed to the pulsing puffs disease. This infusion is a disease effect. When using a wood kinetic blasts against a creature infected by this infusion, you gain a +2 bonus on attack rolls, to saving throw DCs, and on caster level checks to overcome spell resistance.',
        id: 'spore',
        prepend: false,
        append: true,
        prependText: null,
        appendText: 'Spore',
        noBlastText: false,
    },
    stylish: {
        name: 'Stylish',
        element: ['universal'],
        type: 'substance',
        level: 3,
        burn: 2,
        prerequisites: ["brawler's-flurry or flurry of blows class feature", 'kinetic-fist'],
        blasts: ['any'],
        save: null,
        description:
            'When you learn this infusion, select an unchained monk style strike. You can use that style strike during a flurry of blows with your infused blast, though you must still follow the limitation of one style strike per flurry of blows (or two if you are a 15th-level unchained monk).\n\nIf you select flying kick, the distance is equal to your elemental overflow bonus on attack rolls × 10 feet. You can use this infusion only while also using the kinetic fist form infusion.\n\nSpecial: You can select this infusion multiple times. Each time, you must select a different style strike.',
        id: 'stylish',
        prepend: true,
        append: false,
        prependText: 'Stylish',
        appendText: null,
    },
    thundering: {
        name: 'Thundering',
        element: ['air'],
        type: 'substance',
        level: 1,
        burn: 1,
        blasts: ['electric', 'thunderstorm'],
        save: 'Fortitude negates',
        description:
            "Your lightning brings with it a peal of thunder. Whenever your infused blast hits a foe and penetrates spell resistance, that foe becomes deafened, even if the blast doesn't deal damage.\n\n*Permanently* deafens on failed save.",
        id: 'thundering',
        prepend: true,
        append: false,
        prependText: 'Thundering',
        appendText: null,
    },
    synaptic: {
        name: 'Synaptic',
        element: ['air'],
        type: 'substance',
        level: 3,
        burn: 2,
        blasts: ['electric', 'charged-water', 'thunderstorm'],
        save: 'Will negates',
        description:
            "Your blast sends strange electric signals through the target's body, scrambling its synapses and causing it to become staggered for [[1]] round. The target is aware it can choose to take a move action to remove the staggered condition inflicted by this infusion. This infusion is a mind-affecting effect.",
        id: 'synaptic',
        prepend: true,
        append: false,
        prependText: 'Synaptic',
        appendText: null,
    },
    toxic: {
        name: 'Toxic',
        element: ['wood'],
        type: 'substance',
        level: 4,
        burn: 3,
        blasts: ['autumn', 'spring', 'summer', 'verdant', 'winter', 'wood'],
        save: 'Fortitude negates',
        description:
            'The plants in your blast are mildly toxic. All creatures that take piercing or slashing damage from your blast are sickened for [[1]] round.',
        id: 'toxic',
        prepend: true,
        append: false,
        prependText: 'Toxic',
        appendText: null,
    },
    'toxic-greater': {
        name: 'Toxic, Greater',
        element: ['wood'],
        type: 'substance',
        level: 7,
        burn: 3,
        prerequisites: ['toxic'],
        blasts: ['autumn', 'spring', 'summer', 'verdant', 'winter', 'wood'],
        save: 'Fortitude negates',
        description:
            'Your plant toxin is more virulent. Each time you use this infusion, choose a physical ability score. All creatures that take piercing or slashing damage from your blast are exposed to your poison.',
        id: 'toxic, greater',
        prepend: true,
        append: false,
        prependText: 'Super-Toxic',
        appendText: null,
    },
    'turning-blast': {
        name: 'Turning Blast',
        description:
            'Your kinetic blast causes undead creatures to flee in terror.\n\nElements void, wood; Type substance infusion; Level 4; Burn 3Prerequisite(s): duskwalker, or Planar InfusionAssociated Blast(s): positive, negativeSaving Throw Will negates\n\nUndead that take damage from a turning blast must succeed at a Will save or flee for [[1]] round. An undead creature that succeeds at its saving throw against a turning blast is immune to this effect for 24 hours.',
        id: 'turning-blast',
        prepend: true,
        append: false,
        prependText: 'Undead-Turning',
        appendText: null,
    },
    'unblinking-flame': {
        name: 'Unblinking Flame',
        element: ['fire'],
        type: 'substance',
        level: 5,
        burn: 3,
        prerequisites: ['kinetic-fist', 'member-of the monastery of unblinking flame'],
        blasts: ['blue-flame', 'fire'],
        save: null,
        description:
            'The flame of your infused blast illuminates and reveals the target as it really is. If you hit the target with an unarmed strike with the infused blast, creatures can see the target as if using true seeing for [[1]] round, although only the target is revealed in this way. You can use this infusion only while also using the kinetic fist form infusion.',
        id: 'unblinking-flame infusion',
        prepend: true,
        append: false,
        prependText: 'Unblinking Flame',
        appendText: null,
    },
    'unbreaking-waves': {
        name: 'Unbreaking Waves',
        element: ['water'],
        type: 'substance',
        level: 5,
        burn: 3,
        prerequisites: ['kinetic-fist', 'member-of the monastery of unbreaking waves'],
        blasts: ['ice', 'water'],
        save: null,
        description:
            'Your infused blast buffets and redirects enemies with waves.\n\nAttempt a reposition or trip combat maneuver check against each target damaged by your infused blast, using your Constitution modifier instead of your Strength modifier to determine your combat maneuver bonus. You can use this infusion only while also using the kinetic fist form infusion.',
        id: 'unbreaking-waves infusion',
        prepend: true,
        append: false,
        prependText: 'Unbreaking Waves',
        appendText: null,
    },
    'unfolding-wind': {
        name: 'Unfolding Wind',
        element: ['air'],
        type: 'substance',
        level: 5,
        burn: 3,
        prerequisites: ['kinetic-fist', 'member-of the monastery of unfolding wind'],
        blasts: ['air', 'thunderstorm'],
        save: null,
        description:
            'Unfolding winds carry you along. Before your first attack and again after each unarmed strike with the infused blast, you can move 5 feet without provoking attacks of opportunity. You can use this infusion only while also using the kinetic fist form infusion.',
        id: 'unfolding-wind infusion',
        prepend: true,
        append: false,
        prependText: 'Unfolding Wind',
        appendText: null,
    },
    'untwisting-iron': {
        name: 'Untwisting Iron',
        element: ['earth'],
        type: 'substance',
        level: 5,
        burn: 3,
        prerequisites: ['flesh-of stone', 'kinetic-fist', 'member-of the monastery of untwisting iron'],
        blasts: ['earth', 'metal'],
        save: null,
        description:
            'Each time you hit a foe with an unarmed strike with your infused blast, increase your DR/adamantine from flesh of stone by 1 until the start of your next turn. You can use this infusion only while also using the kinetic fist form infusion.',
        id: 'untwisting-iron',
        prepend: true,
        append: false,
        prependText: 'Untwisting Iron',
        appendText: null,
    },
    unnerving: {
        name: 'Unnerving',
        element: ['void'],
        type: 'substance',
        level: 3,
        burn: 2,
        blasts: ['negative', 'void'],
        save: 'Will negates',
        description:
            "Your kinetic blast sends the fear of oblivion into your foes. Whenever an infused blast deals negative energy damage to a living foe, it is shaken for [[1]] round. This shaken condition doesn't stack with itself.",
        id: 'unnerving',
        prepend: true,
        append: false,
        prependText: 'Unnerving',
        appendText: null,
    },
    unraveling: {
        name: 'Unraveling',
        element: ['fire'],
        type: 'substance',
        level: 5,
        burn: 3,
        blasts: ['blue-flame', 'fire'],
        save: null,
        description:
            "Your kinetic blast burns so hot that it melts away your foe's magical effects. Whenever your infused blast hits a foe and penetrates its spell resistance, you can attempt a caster level check as if using a targeted dispel magic before determining whether the foe takes damage from the blast. You can choose to reduce the blast's damage by half to increase your bonus on the caster level check by 2. If you don't choose a specific spell effect to attempt to dispel, you automatically attempt to dispel effects that would protect the target from fire damage before other spells.",
        id: 'unraveling',
        prepend: true,
        append: false,
        prependText: 'Unraveling',
        appendText: null,
    },
    vampiric: {
        name: 'Vampiric',
        element: ['void'],
        type: 'substance',
        level: 5,
        burn: 3,
        prerequisites: ['void-healer'],
        blasts: ['negative', 'void'],
        save: null,
        description:
            "Your kinetic blast can drain your foes' vitality to replenish your own. If your blast hits (or the enemy fails its saving throw against a blast without an attack roll), you can activate the void healer utility wild talent on yourself by accepting its burn cost; you don't need to take an action to do so. The void healer utility wild talent heals you when used in this way even if you are a living creature.",
        id: 'vampiric',
        prepend: true,
        append: false,
        prependText: 'Vampiric',
        appendText: null,
    },
    weighing: {
        name: 'Weighing',
        element: ['void'],
        type: 'substance',
        level: 2,
        burn: 2,
        blasts: ['gravity', 'void'],
        save: 'Reflex negates',
        description:
            'This infusion functions as entangling infusion, except it entangles and immobilizes a foe by increasing its weight, rather than surrounding it in elemental matter.',
        id: 'weighing',
        prepend: true,
        append: false,
        prependText: 'Weighing',
        appendText: null,
    },
};
