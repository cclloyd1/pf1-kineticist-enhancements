export const metakinesis = {
    minimize: {
        id: 'minimize',
        name: 'Minimize',
        burn: '0',
        description: 'Minimizes damage to your blast.',
    },
    empower: {
        id: 'empower',
        name: 'Empower',
        burn: '1',
        description: 'As per the Empower Spell metamagic feat.',
    },
    maximize: {
        id: 'maximize',
        name: 'Maximize',
        burn: '2',
        description: 'As per the Maximize Spell metamagic feat.',
    },
    quicken: {
        id: 'quicken',
        name: 'Quicken',
        burn: '3',
        description: 'As per the Quicken Spell metamagic feat.',
    },
    twice: {
        id: 'twice',
        name: 'Twice',
        burn: '4',
        description:
            'When casting your blast, you can cast it twice with the same standard action (or swift action if using quicken).',
    },
};
